# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_security_group
resource "azurerm_network_security_group" "vnet1_subnet1" {
  name                = "sb-default-nsg"
  location            = var.location
  resource_group_name = azurerm_resource_group.vnet1.name
}

# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_security_rule

resource "azurerm_network_security_rule" "ssh" {
  name                        = "allow-ssh-from-office"
  priority                    = 100
  direction                   = "Inbound"
  access                      = "Allow"
  protocol                    = "Tcp"
  source_port_range           = "*"
  destination_port_range      = "22"
  source_address_prefix       = "77.100.91.26"
  destination_address_prefix  = "10.0.1.4"
  resource_group_name         = azurerm_resource_group.vnet1.name
  network_security_group_name = azurerm_network_security_group.vnet1_subnet1.name
}

# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet_network_security_group_association
resource "azurerm_subnet_network_security_group_association" "vnet1_subnet1" {
  subnet_id                 = azurerm_subnet.vnet1["subnet1"].id
  network_security_group_id = azurerm_network_security_group.vnet1_subnet1.id
}
