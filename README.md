## Name

Azure Terraform Pipelines

## Goals

- [x] Create a CI/CD pipeline with mandatory "Success" steps - [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
  - [x] Improve bootstrap step - make it always run, and note `az` is idempotent
- [x] ~~Develop an approvals-based MR process - not possible with Free plan. Others too expensive to justify.~~
- [x] Protect Main from direct commits
- [x] Deploy/Modify Azure infrastructure with Terraform - (SP with Contributor used.)
- [ ] Migrate to OpenTofu - https://opentofu.org/docs/
- [ ] Develop a "Baseline" configuration which is enforced on a schedule?
- [ ] Implement `tfsec`
- [ ] Get some AKS/Kubernetes/Helm hype going
  - [ ] Services/Ingresses/Nginx - refresh my k8s networking knowledge
- [ ] Firewall tests?
- [ ] Create some template-based pipelines a'la Emp with url pulls and caching of scripts etc.

## Description

Looking to create a CI/CD infrastructure deployment pipeline utilising Terraform, based on vendor best-practices, serving as my "notebook" for model GitLab CI/IaC deployments.
