# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/route_table
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet_route_table_association

resource "azurerm_route_table" "internet" {
  name                = "internet-routetable"
  location            = var.location
  resource_group_name = azurerm_resource_group.vnet1.name

  route {
    name                   = "internet-via-fw"
    address_prefix         = "0.0.0.0/0"
    next_hop_type          = "VirtualAppliance"
    next_hop_in_ip_address = "10.0.2.4" #TODO: Can we target the FW Private IP Programatically?
  }
}

resource "azurerm_subnet_route_table_association" "internet" {
  subnet_id      = azurerm_subnet.vnet1["subnet1"].id
  route_table_id = azurerm_route_table.internet.id
}
