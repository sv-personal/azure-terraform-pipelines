# Resource Group for Firewall Resources
resource "azurerm_resource_group" "firewall" {
  name     = "sb-firewall-rg"
  location = var.location
}

# Resource Group for vnet
resource "azurerm_resource_group" "vnet1" {
  name     = "sb-vnet1-rg"
  location = var.location
}
