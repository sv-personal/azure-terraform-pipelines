# https://developer.hashicorp.com/terraform/language/expressions/type-constraints
# https://developer.hashicorp.com/terraform/language/functions/try
# By declaring the variable components as optional and providing defaults, we avoid the necessity of using the
# try Function for optional components in the for_each blocks, because errors will no longer occur when values are
# not passed in

variable "rule_collection_groups" {
  type = map(object({
    name        = string
    priority    = number
    description = optional(string, "")
    nat_rule_collections = optional(map(object({
      name     = string
      priority = number
      action   = string
      rules = optional(map(object({
        name                = string
        description         = optional(string)
        protocols           = list(string)
        source_addresses    = optional(list(string))
        destination_address = optional(string)
        source_ip_groups    = optional(list(string))
        destination_ports   = list(string)
        translated_address  = optional(string)
        translated_fqdn     = optional(string)
        translated_port     = string
      })), {})
    })), {})
    network_rule_collections = optional(map(object({
      name     = string
      priority = number
      action   = string
      rules = optional(map(object({
        name                  = string
        description           = optional(string)
        protocols             = list(string)
        source_addresses      = optional(list(string))
        source_ip_groups      = optional(list(string))
        destination_addresses = optional(list(string))
        destination_fqdns     = optional(list(string))
        destination_ip_groups = optional(list(string))
        destination_ports     = list(string)
      })), {})
    })), {})
    application_rule_collections = optional(map(object({
      name     = string
      priority = number
      action   = string
      rules = optional(map(object({
        name        = string
        description = optional(string)
        protocols = optional(map(object({
          type = string
          port = number
        })), {})
        source_addresses      = list(string)
        source_ip_groups      = optional(list(string))
        destination_fqdns     = optional(list(string))
        destination_fqdn_tags = optional(list(string))
      })), {})
    })), {})
  }))
  default = {
    rcg_001 = {
      name     = "rcg_001"
      priority = 10000
      network_rule_collections = {
        nrc_001 = {
          name     = "nrc_001"
          priority = 200
          action   = "Allow"
          rules = {
            allow_google_dns = {
              name                  = "allow_google_dns"
              protocols             = ["UDP"]
              source_addresses      = ["*"]
              destination_addresses = ["8.8.8.8", "8.8.4.4"]
              destination_ports     = ["53"]
            }
          }
        }
      }
      application_rule_collections = {
        arc_001 = {
          name     = "arc_001"
          priority = 300
          action   = "Allow"
          rules = {
            allow_google_dns = {
              name              = "allow_youtube"
              source_addresses  = ["*"]
              destination_fqdns = ["*.youtube.com"]
              protocols = {
                Https = {
                  type = "Https"
                  port = 443
                }
              }
            }
          }
        }
      }
    }
  }
}
