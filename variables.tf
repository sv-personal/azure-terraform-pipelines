variable "location" {
  default     = "uksouth"
  description = "Location of resources - set in GitLab CI Vars"
}

variable "prefix" {
  type        = string
  default     = "sbfwpoltest"
  description = "Prefix to attach to resource names"
}

variable "vnet1_address_spaces" {
  type = list(string)
  default = [
    "10.0.0.0/16"
  ]
  description = "Address space(s) to allocate to vnet1"
}

#TODO: Will use this var later with a subnet loop and separate NSG assignment block
#NOTE: We may see an issue here as we are passing a list in to a for_each - might need to change it to a string
# and hit it with a tolist() at the loop stage
variable "vnet1_subnets" {
  type = map(any)
  default = {
    subnet1 = {
      name             = "subnet1"
      address_prefixes = ["10.0.1.0/24"]
    }
    subnet2 = {
      name             = "AzureFirewallSubnet"
      address_prefixes = ["10.0.2.0/26"]
    }
    subnet3 = {
      name             = "AzureFirewallManagementSubnet"
      address_prefixes = ["10.0.3.0/26"]
    }
  }
  description = "Address space(s) to allocate to vnet1"
}

variable "vm_username" {
  type        = string
  default     = ""
  description = "Admin username for VM"
}

variable "vm_password" {
  type        = string
  default     = ""
  description = "Admin password for VM"
}
