#https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_machine
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/public_ip

# resource "azurerm_public_ip" "test_vm_1" {
#   name                = "${var.prefix}-pip"
#   resource_group_name = azurerm_resource_group.vnet1.name
#   location            = var.location
#   allocation_method   = "Static"

#   tags = local.tags

#   lifecycle {
#     # https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/public_ip
#     create_before_destroy = true
#   }
# }

# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_interface
resource "azurerm_network_interface" "test_vm_1" {
  name                = "${var.prefix}-nic"
  location            = var.location
  resource_group_name = azurerm_resource_group.vnet1.name

  ip_configuration {
    name = "ipconfig1"
    # subnet_id                     = tolist(azurerm_virtual_network.vnet1.subnet)[0].id
    subnet_id                     = azurerm_subnet.vnet1["subnet1"].id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_virtual_machine" "test_vm_1" {
  name                  = "${var.prefix}-vm"
  location              = var.location
  resource_group_name   = azurerm_resource_group.vnet1.name
  network_interface_ids = [azurerm_network_interface.test_vm_1.id]
  vm_size               = "Standard_DS1_v2"

  # Uncomment this line to delete the OS disk automatically when deleting the VM
  delete_os_disk_on_termination = true

  # Uncomment this line to delete the data disks automatically when deleting the VM
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "fw-test-vm"
    admin_username = var.vm_username
    admin_password = var.vm_password
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  tags = local.tags
}
