output "vnet1" {
  value = azurerm_virtual_network.vnet1
}

output "firewall_public_ip" {
  value = azurerm_public_ip.fw_transit_ip.ip_address
}
