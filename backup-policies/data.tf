# We need to grab some data for the dynamic rules in the Firewall here.

# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/virtual_machine
data "azurerm_virtual_machine" "test_vm" {
  name                = "${var.prefix}-vm"
  resource_group_name = "sb-vnet1-rg"
}

# data.azurerm_virtual_machine.test_vm.private_ip_address

# Getting the Public IP of the Firewall
# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/public_ip
data "azurerm_public_ip" "fw_transit_ip" {
  name                = "FWBasicTransitIP"
  resource_group_name = "sb-vnet1-rg"
}

# data.azurerm_public_ip.fw_transit_ip.ip_address
