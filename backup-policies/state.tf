terraform {
  # If SP has "Terraform" or "Contributor" role scoped to Subscription level, this works fine.
  backend "azurerm" {
    resource_group_name  = "terraform-backend-storage"
    storage_account_name = "sbtfbackend"
    container_name       = "tfstate"
    key                  = "terraform-backup.tfstate"
  }
}
