#!/usr/bin/env bash

set -euo pipefail
trap "echo 'error: Script failed: see failed command above'" ERR

message='copying supporting files from parent folder'
echo "Started $message..."
cp ../scripts/* ./scripts
cp ../versions.tf .
cp ../variables.tf .
cp ../locals.tf .
cp ../firewall-rules.tf .
echo "Finished $message."
