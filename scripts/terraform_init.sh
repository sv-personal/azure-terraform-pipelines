#!/usr/bin/env bash

# Stop the script if any commands fail
set -euo pipefail
trap "echo 'error: Script failed: see failed command above'" ERR

terraform init
