#!/usr/bin/env bash

# Is it already created?
# Checking above pipefail line because this exits with error if account not found.
message="Checking for Storage Account [ $TERRAFORM_STORAGE_RG\\$TERRAFORM_STORAGE_ACCOUNT ]."
echo "STARTED: $message"
storage_account=$(az storage account show --name "$TERRAFORM_STORAGE_ACCOUNT" --resource-group "$TERRAFORM_STORAGE_RG")
echo "FINISHED: $message"

# Stop the script if any commands fail
set -euo pipefail
trap "echo 'error: Script failed: see failed command above'" ERR

if [ -z "${storage_account}" ]; then

    echo "[ $TERRAFORM_STORAGE_RG\\$TERRAFORM_STORAGE_ACCOUNT ] not found..."

    message="Creating Resource Group."
    # Resource Group
    echo "STARTED: $message"
    az group create --location "$TF_VAR_location" --name "$TERRAFORM_STORAGE_RG"
    echo "FINISHED: $message"

    # Storage Account
    message="Creating Storage Account."
    echo "STARTED: $message"
    az storage account create --name "$TERRAFORM_STORAGE_ACCOUNT" --resource-group "$TERRAFORM_STORAGE_RG" --location "$TF_VAR_location" --sku Standard_LRS
    echo "FINISHED: $message"

    # Storage Container
    message="Creating Storage Container."
    echo "STARTED: $message"
    az storage container create --name "$TERRAFORM_STORAGE_CONTAINER_NAME" --account-name "$TERRAFORM_STORAGE_ACCOUNT"
    echo "FINISHED: $message"

else
    echo "[ $TERRAFORM_STORAGE_RG\\$TERRAFORM_STORAGE_ACCOUNT ] exists! Nothing to do..."

fi
