# Actions

- Fixes the foo issue
- Changes the bar layout
- Adds some bants

# Related Issues

<!--  Change the issue numbers below to correspond to any fixed/closed issue(s) -->
- Fixes #ISSUE_NUMBER
- Closes #ISSUE_NUMBER

<!-- DO NOT CHANGE ANYTHING BELOW THIS LINE -->

# Engineer Checks

- [ ] Merge Request description provides enough context for the reviewer
- [ ] The changes listed fit the description given
- [ ] Pipeline shows as successful/successful with warnings
- [ ] Plan checked for unwanted additions/changes/deletions
- [ ] A reviewer has been requested for this MR

# Reviewer Pre-Approval Checks

- [ ] The changes listed fit the description given
- [ ] Pipeline shows as  successful/successful with warnings
- [ ] Plan checked for unwanted additions/changes/deletions
