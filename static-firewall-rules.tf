resource "azurerm_firewall_policy_rule_collection_group" "static" {

  name               = "static-rcg"
  firewall_policy_id = azurerm_firewall_policy.test_firewall.id
  priority           = 11000

  nat_rule_collection {
    name     = "natrc_001"
    priority = 100
    action   = "Dnat"
    rule {
      name = "allow_ssh_in"
      protocols = [
        "TCP"
      ]
      source_addresses = [
        "86.148.197.223",
        "77.100.91.26"
      ]
      destination_address = azurerm_public_ip.fw_transit_ip.ip_address
      destination_ports = [
        "22"
      ]
      translated_address = azurerm_network_interface.test_vm_1.ip_configuration[0].private_ip_address
      translated_port    = "22"
    }
  }
}
