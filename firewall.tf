# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/firewall

resource "azurerm_public_ip" "fw_transit_ip" {
  name                = "FWBasicTransitIP"
  location            = var.location
  resource_group_name = azurerm_resource_group.vnet1.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_public_ip" "fw_management_ip" {
  name                = "FWBasicManagementIP"
  location            = var.location
  resource_group_name = azurerm_resource_group.vnet1.name
  allocation_method   = "Static"
  sku                 = "Standard"
}

resource "azurerm_firewall" "test_firewall" {
  name                = "sb-test-firewall"
  location            = var.location
  resource_group_name = azurerm_resource_group.vnet1.name
  sku_name            = "AZFW_VNet"
  sku_tier            = "Basic"
  firewall_policy_id  = azurerm_firewall_policy.test_firewall.id

  ip_configuration {
    name                 = "ip-configuration"
    subnet_id            = azurerm_subnet.vnet1["subnet2"].id
    public_ip_address_id = azurerm_public_ip.fw_transit_ip.id
  }

  management_ip_configuration {
    name                 = "management-ip-configuration"
    subnet_id            = azurerm_subnet.vnet1["subnet3"].id
    public_ip_address_id = azurerm_public_ip.fw_management_ip.id
  }
}
