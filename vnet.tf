# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_network

resource "azurerm_virtual_network" "vnet1" {

  name                = "vnet1"
  location            = var.location
  resource_group_name = azurerm_resource_group.vnet1.name
  address_space       = var.vnet1_address_spaces

  # subnet {
  #   name           = "subnet1"
  #   address_prefix = "10.0.1.0/24"
  #   security_group = azurerm_network_security_group.vnet1_subnet1.id
  # }

  # subnet {
  #   name           = "AzureFirewallSubnet"
  #   address_prefix = "10.0.2.0/26"
  # }

  tags = local.tags
}

# https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet

resource "azurerm_subnet" "vnet1" {
  for_each = var.vnet1_subnets

  name                 = each.value.name
  address_prefixes     = each.value.address_prefixes
  resource_group_name  = azurerm_resource_group.vnet1.name
  virtual_network_name = azurerm_virtual_network.vnet1.name

}
